package linearSortv2

import (
	"fmt"
	"sync"
)

const (
	ARRAY_SKIP = iota
	ARRAY_CELL
	ARRAY_OUTPUT
)

type Arraycell struct {
	Wg        *sync.WaitGroup
	jumpTable map[int]func() int
	jump      int
	proxy     chan struct{}
	arraySize int
	id        int
	store     int
	count     int
	DigitIn   chan int
	DigitOut  chan int
}

func (a *Arraycell) Arraycell(id int, arraysize int) {
	a.jumpTable = map[int]func() int{
		ARRAY_CELL:   a.cell,
		ARRAY_OUTPUT: a.output,
	}
	a.id = id
	a.arraySize = arraysize
	a.store = 0
	a.count = 0
	a.proxy = make(chan struct{}, 1)
	a.proxy <- struct{}{}
	a.jump = ARRAY_CELL
	a.Wg.Add(1)
	go func() {
		for {
			a.jump = a.jumpTable[a.jump]()
			if a.jump == ARRAY_SKIP {
				break
			}
		}
		a.Wg.Done()
	}()
}

func (a *Arraycell) cell() int {
	select {
	case <-guardedEmptyStructChan(a.count == a.arraySize-a.id, a.proxy):
		a.proxy <- struct{}{}
		return ARRAY_OUTPUT
	case x := <-guardedIntChan(a.count == 0, a.DigitIn):
		a.id = a.id
		a.store = x
		a.count = a.count + 1
		return ARRAY_CELL
	case x := <-guardedIntChan(a.count > 0 && a.count < a.arraySize-a.id, a.DigitIn):
		if x > a.store {
			a.DigitOut <- x
			a.id = a.id
			a.store = a.store
			a.count = a.count + 1
			return ARRAY_CELL
		} else {
			a.DigitOut <- a.store
			a.id = a.id
			a.store = x
			a.count = a.count + 1
			return ARRAY_CELL
		}
	}
}

func (a *Arraycell) output() int {
	select {
	case guardedIntChan(a.count < a.arraySize, a.DigitOut) <- a.store:
		x := <-a.DigitIn
		a.id = a.id
		a.store = x
		a.count = a.count + 1
		return ARRAY_OUTPUT
	case guardedIntChan(a.count == a.arraySize, a.DigitOut) <- a.store:
		a.id = a.id
		a.store = 0
		a.count = a.count + 1
		return ARRAY_OUTPUT
	case <-guardedEmptyStructChan(a.count == a.arraySize+1, a.proxy):
		a.proxy <- struct{}{}
		a.id = a.id
		a.store = 0
		a.count = 0
		return ARRAY_SKIP
	}
}

func guardedIntChan(b bool, c chan int) chan int {
	if !b {
		return nil
	}
	return c
}

func guardedEmptyStructChan(b bool, c chan struct{}) chan struct{} {
	if !b {
		return nil
	}
	return c
}

func Array(wg *sync.WaitGroup, chans []chan int, size int) {
	var array []Arraycell
	for i := 0; i < size+1; i++ {
		chans = append(chans, make(chan int))
	}
	fmt.Println("Channel array has been made")
	for i := 0; i < size; i++ {
		array = append(array,
			Arraycell{Wg: wg, DigitIn: chans[i], DigitOut: chans[i+1]})
		array[i].Arraycell(i, size)
	}
}
