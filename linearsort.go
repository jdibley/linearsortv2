package linearSortv2

import "sync"

func Linearsort(ownerWg *sync.WaitGroup, input chan int, output chan int, size int) {
	ownerWg.Add(1)
	go func() {
		var wg sync.WaitGroup
		var chans []chan int
		for i := 0; i < size+1; i++ {
			chans = append(chans, make(chan int))
		}

		arrayif := Arrayif{Wg: &wg, Send: chans[0], Receive: chans[size],
			Input: input, Output: output}

		Array(&wg, chans, size)
		arrayif.Arrayif(size)

		wg.Wait()
	}()

	ownerWg.Done()
}
