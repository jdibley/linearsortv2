package linearSortv2

import "sync"

const (
	ARRAYIF_SKIP = iota
	ARRAYIF_INSERT
	ARRAYIF_RETRIEVE
)

type Arrayif struct {
	Wg        *sync.WaitGroup
	jumpTable map[int]func() int
	jump      int
	arraysize int
	count     int
	proxy     chan struct{}
	Input     chan int
	Output    chan int
	Send      chan int
	Receive   chan int
}

func (i *Arrayif) Arrayif(n int) {
	i.jumpTable = map[int]func() int{
		ARRAYIF_INSERT:   i.insert,
		ARRAYIF_RETRIEVE: i.retrieve,
	}
	i.proxy = make(chan struct{}, 1)
	i.proxy <- struct{}{}
	i.arraysize = n
	i.count = n
	i.jump = ARRAYIF_INSERT
	i.Wg.Add(1)
	go func() {
		for {
			i.jump = i.jumpTable[i.jump]()
			if i.jump == ARRAYIF_SKIP {
				break
			}
		}
		i.Wg.Done()
	}()
}

func (i *Arrayif) insert() int {
	select {
	case <-guardedEmptyStructChan(i.count == 0, i.proxy):
		i.proxy <- struct{}{}
		i.count = i.arraysize
		return ARRAYIF_RETRIEVE
	case x := <-guardedIntChan(i.count > 0, i.Input):
		i.Send <- x
		i.count = i.count - 1
		return ARRAYIF_INSERT
	}
}

func (i *Arrayif) retrieve() int {
	select {
	case <-guardedEmptyStructChan(i.count == 0, i.proxy):
		i.proxy <- struct{}{}
		return ARRAYIF_SKIP
	case x := <-guardedIntChan(i.count > 0, i.Receive):
		i.Output <- x
		i.count = i.count - 1
		return ARRAYIF_RETRIEVE
	}
}
